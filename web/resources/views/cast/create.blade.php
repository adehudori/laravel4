@extends('adminlte.master')
@section('judul')
Tambah Cast 
@endsection

@section ('content')

        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label> Nama </label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label> Umur </label>
                <input type="text" class="form-control" name="umur" placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label> Biodata </label>
                <textarea name="bio"  class= form-control></textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>


            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>

@endsection